import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-coursecategories',
  templateUrl: '../template/edit-coursecategories.component.html',
  styleUrls: ['../css/coursecategories.component.css']
})
export class EditCourseCategoriesComponent implements OnInit {

  coursecategoriesForm! :FormGroup

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
  ) { }

  ngOnInit(): void {

    this.coursecategoriesForm = this.formBuilder.group({
      image: ["",Validators.required],
      coursecategoriesName: ["", Validators.required],
    });

  }

  coursecategoriesData(val:any){
    console.log(val.value)
    this.router.navigate(['/coursecategories']);

  }

}
