import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-courses',
  templateUrl: '../template/courses.component.html',
  styleUrls: ['../css/courses.component.css']
})
export class CoursesComponent implements OnInit {

  @Output() scroll: EventEmitter<any> = new EventEmitter();

  constructor() {}

  ngOnInit(): void {
  }
}
