import { NavcontentModule } from 'src/app/navcontent/navcontent.module';
import { TutorialsService } from './service/tutorials.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TutorialsRoutingModule } from './tutorials-routing.module';
import {CdkAccordionModule} from '@angular/cdk/accordion';
import {MatExpansionModule} from '@angular/material/expansion';
import { TutorialsComponent } from './component/tutorials.component';
import { HttpClientModule } from '@angular/common/http';
import { PdfViewerModule } from 'ng2-pdf-viewer';


@NgModule({
  declarations: [
    TutorialsComponent
  ],
  imports: [
    CommonModule,
    TutorialsRoutingModule,
    CdkAccordionModule,
    MatExpansionModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    PdfViewerModule,
    NavcontentModule
  ],
  providers: [
    TutorialsService
  ]
})
export class TutorialsModule { }
