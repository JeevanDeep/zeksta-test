import { DashbordComponent } from './dashboardcomponent/dashboard.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { NavcontentModule } from 'src/app/navcontent/navcontent.module';



@NgModule({
  declarations: [
    DashbordComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    NavcontentModule
  ]
})
export class DashboardModule { }
