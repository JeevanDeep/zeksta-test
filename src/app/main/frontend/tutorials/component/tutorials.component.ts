import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TutorialsService } from '../service/tutorials.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-tutorials',
  templateUrl: '../template/tutorials.component.html',
  styleUrls: ['../css/tutorials.component.css']
})
export class TutorialsComponent implements OnInit {

  form1!: FormGroup;
  public paginationNext = true;
  public paginationPrevious = true;
  public paginationFinal = false;
  public showMenu = false;
  public condition = false;
  tutorialId: any;
  topicId: any;
  tutorialsData: any;
  topicDetails: any;
  topicArr= new Array();
  getTopic : any;
  isStore: any;
  public toggleNavbar() {
    this.showMenu = true;
  }

  videos = [
    { 'id': 1, 'name': 'Introduction', 'url': "../../../../assets/images/Solid Logo Reveal Preview 69137360.mp4" },
    { 'id': 2, 'name': 'chapter 1', 'url': "../../../../assets/images/Solid Logo Reveal Preview 69137360.mp4" },
    { 'id': 3, 'name': 'chapter 2', 'url': "../../../../assets/images/Solid Logo Reveal Preview 69137360.mp4" },
    { 'id': 4, 'name': 'chapter 3', 'url': "../../../../assets/images/Solid Logo Reveal Preview 69137360.mp4" },
  ];

  topic = [
    { 'id': 1, 'slug': 'introduction', 'name': 'Introduction' },
    { 'id': 2, 'slug': 'chapter-1', 'name': 'Chapter 1' },
    { 'id': 3, 'slug': 'chapter-2', 'name': 'Chapter 2' },
    { 'id': 4, 'slug': 'chapter-3', 'name': 'Chapter 3' },
    { 'id': 5, 'slug': 'chapter-4', 'name': 'Chapter 4' },
    { 'id': 6, 'slug': 'chapter-5', 'name': 'Chapter 5' },
    { 'id': 7, 'slug': 'chapter-6', 'name': 'Chapter 6' },
    { 'id': 8, 'slug': 'chapter-7', 'name': 'Chapter 7' },
    { 'id': 9, 'slug': 'chapter-8', 'name': 'Chapter 8' },
    { 'id': 10, 'slug': 'chapter-9', 'name': 'Chapter 9' },
    { 'id': 11, 'slug': 'chapter-10', 'name': 'Chapter 10' },
    { 'id': 12, 'slug': 'chapter-11', 'name': 'Chapter 11' },
  ]



  modalOpen: boolean = true;
  openForm() {
    this.modalOpen = !this.modalOpen;
  }

  constructor(
    private formBuilder: FormBuilder,
    private api: TutorialsService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  topicDetailsObj: any =
    [
      {
        "name": "Introduction",
        "slug": "introduction",
        "data":
          [
            // { 'url': "../../../../assets/images/Solid Logo Reveal Preview 69137360.mp4" },
            { 'pdf': "../../../../assets/document/1.pdf" },
            // { "propertyName": "Property Two" }
          ]
      },
      {
        "name": "Chapter 1",
        "slug": "chapter-1",
        "data":
          [
            // { 'url': "../../../../assets/images/HAPPY.mp4" },
            { 'pdf': "../../../../assets/document/2.pdf" },
            // { "propertyName": "Property Four" },
            // { "propertyName": "Property Five" },
          ]
      },
      {
        "name": "Chapter 2",
        "slug": "chapter-2",
        "data":
          [
            // { 'url': "../../../../assets/images/TCPMaster.mp4" },
            { 'pdf': "../../../../assets/document/3.pdf" },
            // { "propertyName": "Property Seven" },
            // { "propertyName": "Property Eight" },
          ]
      },
      {
        "name": "Chapter 3",
        "slug": "chapter-3",
        "data":
          [
            { 'pdf': "../../../../assets/document/4.pdf" },
            // { "propertyName": "Property Seven" },
            // { "propertyName": "Property Eight" },
          ]
      },
      {
        "name": "Chapter 4",
        "slug": "chapter-4",
        "data":
          [
            { 'pdf': "../../../../assets/document/5.pdf" },
            // { "propertyName": "Property Seven" },
            // { "propertyName": "Property Eight" },
          ]
      },
      {
        "name": "Chapter 5",
        "slug": "chapter-5",
        "data":
          [
            { 'pdf': "../../../../assets/document/6.pdf" },
            // { "propertyName": "Property Seven" },
            // { "propertyName": "Property Eight" },
          ]
      },
      {
        "name": "Chapter 6",
        "slug": "chapter-6",
        "data":
          [
            { 'pdf': "../../../../assets/document/7.pdf" },
            // { "propertyName": "Property Seven" },
            // { "propertyName": "Property Eight" },
          ]
      },
      {
        "name": "Chapter 7",
        "slug": "chapter-7",
        "data":
          [
            { 'pdf': "../../../../assets/document/8.pdf" },
            // { "propertyName": "Property Seven" },
            // { "propertyName": "Property Eight" },
          ]
      },
      {
        "name": "Chapter 8",
        "slug": "chapter-8",
        "data":
          [
            { 'pdf': "../../../../assets/document/9.pdf" },
            // { "propertyName": "Property Seven" },
            // { "propertyName": "Property Eight" },
          ]
      },
      {
        "name": "Chapter 9",
        "slug": "chapter-9",
        "data":
          [
            { 'pdf': "../../../../assets/document/10.pdf" },
            // { "propertyName": "Property Seven" },
            // { "propertyName": "Property Eight" },
          ]
      },
      {
        "name": "Chapter 10",
        "slug": "chapter-10",
        "data":
          [
            { 'pdf': "../../../../assets/document/11.pdf" },
            // { "propertyName": "Property Seven" },
            // { "propertyName": "Property Eight" },
          ]
      },
      {
        "name": "Chapter 11",
        "slug": "chapter-11",
        "data":
          [
            { 'pdf': "../../../../assets/document/12.pdf" },
            // { "propertyName": "Property Seven" },
            // { "propertyName": "Property Eight" },
          ]
      }
    ];




  ngOnInit() {
    this.route.params.subscribe(params => {
      this.tutorialId = params['tutorials'];
      this.topicId = params['topic'];
    });
    this.getTutorialsDetails(this.topicId);
    // console.log(99, this.topicId);
    // console.log(100, this.topicDetails);
    this.getTopic = localStorage.getItem('deletedItems');
    this.isStore = this.getTopic ? JSON.parse(this.getTopic).length: 0;
    (this.getTopic == null || this.isStore == 0) ? localStorage.setItem('deletedItems', JSON.stringify(this.topicArr)):'';
    this.getTopic = localStorage.getItem('deletedItems');



    this.form1 = this.formBuilder.group({
      name: ["", [Validators.required]],
      mobile: ["", Validators.required],
      email: ["", Validators.required],
    });

  }

  submitForm1(val: any) {
    this.api.postMessage(val.value).subscribe(response => {
      location.href = 'https://mailthis.to/confirm'
      console.log(response)
    }, error => {
      console.warn(error.responseText)
      console.log({ error })
    })
  }

  redirectUrlByTopic(tutorialId: any, topic: any) {
    this.condition = true;
    this.getTutorialsDetails(topic);
    this.getTopic = localStorage.getItem('deletedItems');
    console.log(225,this.getTopic )
    this.router.navigate(['/tutorials/' + tutorialId + '/' + topic])
  }


  nextTopic() {
    this.topicDetailsObj.map((element: any, key: any) => {
      if (element.slug == this.topicId) {
        console.log(232, this.topicDetailsObj.length, "//", key)
        if (key == this.topicDetailsObj.length - 1) {
          this.paginationNext = false;
          this.paginationFinal = true;
        } else {

          this.getTopic = localStorage.getItem('deletedItems');
          this.getTopic = JSON.parse(this.getTopic).map((element:any) => {
            // console.log(248, element);
            this.topicArr.push(element);
          })


          this.topicArr.push(this.topicId);
          // const getTopicArray = JSON.parse(this.getTopic)
          // console.log(240,JSON.parse(this.getTopic) );
          localStorage.setItem('deletedItems', JSON.stringify(this.topicArr))
          this.paginationPrevious = true;
          const topicName = this.topicDetailsObj[key + 1].slug
          this.getTutorialsDetails(topicName);
          this.router.navigate(['/tutorials/' + this.tutorialId + '/' + topicName])
        }
      }
    });
  }

  previousTopic() {
    this.topicDetailsObj.map((element: any, key: any) => {
      if (element.slug == this.topicId) {
        console.log(250, this.topicDetailsObj.length, "//", key)
        if (key == 0) {
          console.log(254,)
          this.paginationPrevious = false;
        } else {
          this.paginationNext = true;
          this.paginationFinal =false;
          const topicName = this.topicDetailsObj[key - 1].slug
          console.log(104, this.topicDetailsObj[key - 1].slug);
          this.getTutorialsDetails(topicName);
          this.router.navigate(['/tutorials/' + this.tutorialId + '/' + topicName])
        }
      }
    });

  }

  // .then(() => {
  //   window.location.reload();
  // });

  getTutorialsDetails(topic: any) {

    // console.log(131, this.topicDetails);
    this.topicDetailsObj.map((element: any) => {
      // console.log(103, element)
      if (element.slug == topic) {
        this.topicDetails = element;

      }
    });
    console.log(135, this.topicDetails);
  }

}
