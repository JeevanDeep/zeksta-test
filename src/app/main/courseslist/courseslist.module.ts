import { AddCourseslistComponent } from './component/add-courseslist.component';
import { EditCourseslistComponent } from './component/edit-courseslist.component';
import { ListCourseslistComponent } from './component/list-courseslist.component';
import { NavcontentModule } from 'src/app/navcontent/navcontent.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule} from '@angular/material/form-field';
import { CoursesListRoutingModule } from './courseslist-routing.module';
import { MatIconModule } from '@angular/material/icon';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { CKEditorModule } from 'ckeditor4-angular';

@NgModule({
  declarations: [
    ListCourseslistComponent,
    EditCourseslistComponent,
    AddCourseslistComponent
  ],
  imports: [
    CommonModule,
    CoursesListRoutingModule,
    NavcontentModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    NgxDropzoneModule,
    CKEditorModule

  ],
  exports:[
    ListCourseslistComponent,
    EditCourseslistComponent,
    AddCourseslistComponent
  ]
})
export class CoursesListModule { }
