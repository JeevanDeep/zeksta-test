import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-add-courseslist',
  templateUrl: '../template/add-courseslist.component.html',
  styleUrls: ['../css/courseslist.component.css']
})
export class AddCourseslistComponent implements OnInit {

  coursesListForm!: FormGroup;
  selectedType = 'opentype';
  images: File[] = [];
  videos: File[] = [];
  documents: File[] = [];
  val:any;
  public editorData = '<p>Hello, world!</p>';


  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.coursesListForm = this.formBuilder.group({
      courseType: ["", Validators.required],
      courseCategoriesName: ["", Validators.required],
      coursesName: ["", Validators.required],
      file: ["", Validators.required],
    });

  }

  coursesListData(val: any) {
    console.log(val.value)
  }



  onChange(event:any) {
    this.selectedType = event.target.value;
  }


  onSelectImage(event: any) {
    console.log(event);
    this.val = this.images.push(...event.addedFiles);
  }


  onRemoveImage(event: any) {
    console.log(event);
    this.images.splice(this.images.indexOf(event), 1);
  }

  onSelectVideo(event: any) {
    console.log(event);
    this.videos.push(...event.addedFiles);
  }
  onRemoveVideo(event: any) {
    console.log(event);
    this.videos.splice(this.videos.indexOf(event), 1);
  }

  onSelectDocument(event: any) {
    console.log(event);
    this.documents.push(...event.addedFiles);
  }

  onRemoveDocument(event: any) {
    console.log(event);
    this.documents.splice(this.documents.indexOf(event), 1);
  }






}
