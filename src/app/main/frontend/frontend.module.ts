import { NavcontentModule } from 'src/app/navcontent/navcontent.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OwlModule } from 'ngx-owl-carousel';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { FrontendRoutingModule } from './frontend-routing.module';
import { HomeComponent } from './home/home.component';
import { RouterModule } from '@angular/router';
import { MatIconModule } from '@angular/material/icon';




@NgModule({
  declarations: [
    HomeComponent,
  ],
  imports: [
    CommonModule,
    FrontendRoutingModule,
    RouterModule,
    CarouselModule,
    OwlModule,
    MatIconModule,

    NavcontentModule
  ],
  exports:[

  ]
})
export class FrontendModule { }
