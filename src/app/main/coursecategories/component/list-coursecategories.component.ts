import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-coursecategories',
  templateUrl: '../template/list-coursecategories.component.html',
  styleUrls: ['../css/coursecategories.component.css']
})
export class ListCourseCategoriesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
