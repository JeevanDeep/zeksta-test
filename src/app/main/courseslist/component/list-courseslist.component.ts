import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-courseslist',
  templateUrl: '../template/list-courseslist.component.html',
  styleUrls: ['../css/courseslist.component.css']
})
export class ListCourseslistComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
