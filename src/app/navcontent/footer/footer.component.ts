import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  @Output() scroll: EventEmitter<any> = new EventEmitter();

  constructor() {}

  ngOnInit(): void {
  }

  scrollToHome() {
    this.scroll.emit(true)
  }

  scrollToAbout() {
    this.scroll.emit(true)
  }

  scrollToPortfolio()
  {
    this.scroll.emit(true)
  }

  scrollToTestimonials()
  {
    this.scroll.emit(true)
  }

  scrollToContact()
  {
    this.scroll.emit(true)
  }

}
