import { CourseDetailsComponent } from './component/courseDetails.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoursesDetailsRoutingModule } from './courses-details-routing.module';
import { AccordionModule } from "ngx-accordion";
import { RouterModule } from '@angular/router';
import { NavcontentModule } from 'src/app/navcontent/navcontent.module';


@NgModule({
  declarations: [
    CourseDetailsComponent
  ],
  imports: [
    CommonModule,
    CoursesDetailsRoutingModule,
    AccordionModule,
    RouterModule,
    NavcontentModule

  ]
})
export class CoursesDetailsModule { }
