import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path:'',component:HomeComponent },
  { path: "courses", loadChildren: () => import(`./courses/courses.module`).then(m => m.CoursesModule)},
  { path: "coursedetails", loadChildren: () => import(`./courses-details/courses-details.module`).then(m => m.CoursesDetailsModule)},
  { path: "tutorials", loadChildren: () => import(`./tutorials/tutorials.module`).then(m => m.TutorialsModule)},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FrontendRoutingModule { }
