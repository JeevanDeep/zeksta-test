import { LoginComponent } from './login/login.component';

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  { path:"",component:LoginComponent},
  { path: "home", loadChildren: () => import(`./frontend/frontend.module`).then(m => m.FrontendModule)},
  { path: "dashboard" , loadChildren: () => import(`./dashboard/dashboard.module`).then(m => m.DashboardModule)},
  { path: "profile", loadChildren: () => import(`./profile/profile.module`).then(m => m.ProfileModule)},
  { path: "coursecategories", loadChildren: () => import(`./coursecategories/coursecategories.module`).then(m => m.CoursecategoriesModule)},
  { path: "courseslist", loadChildren: () => import(`./courseslist/courseslist.module`).then(m => m.CoursesListModule)},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
