import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class loginAuthGuard implements CanActivate {
  constructor(private router: Router){}
  canActivate(){
    if(localStorage.getItem('adminUserSession')){
      return true;
    }else{
      this.router.navigate(['/login']);
      return false;
    }
  }
}

@Injectable({
  providedIn: 'root'
})
export class logoutAuthGuard implements CanActivate {
  constructor(private router: Router){}
  canActivate(){
    if(!localStorage.getItem('adminUserSession')){
      return true;
    }else{
      this.router.navigate(['/']);
      return false;
    }
  }
}

// canActivate(
  //   route: ActivatedRouteSnapshot,
  //   state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
  //   return true;
  // }
