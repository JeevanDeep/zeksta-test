import { NavcontentModule } from 'src/app/navcontent/navcontent.module';

import { CoursesComponent } from './component/courses.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { CoursesRoutingModule } from './courses-routing.module';


@NgModule({
  declarations: [
    CoursesComponent,

  ],
  imports: [
    CommonModule,
    CoursesRoutingModule,
    MatIconModule,
    NavcontentModule
  ]
})
export class CoursesModule { }
