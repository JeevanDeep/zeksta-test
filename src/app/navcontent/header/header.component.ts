import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public showMenu = false;
  public showAbout = false;

  public toggleNavbar(){
    this.showMenu = !this.showMenu;
  }

  public aboutbar(){
    this.showAbout = !this.showAbout;
  }

  constructor() { }

  @Output() scroll: EventEmitter<any> = new EventEmitter();

  ngOnInit(): void {
  }

  scrollToHome() {
    this.scroll.emit(true)
  }

  scrollToAbout() {
    this.scroll.emit(true)
  }

  scrollToPortfolio()
  {
    this.scroll.emit(true)
  }

  scrollToTestimonials()
  {
    this.scroll.emit(true)
  }

  scrollToContact()
  {
    this.scroll.emit(true)
  }


}
