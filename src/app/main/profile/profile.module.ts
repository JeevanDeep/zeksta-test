import { NavcontentModule } from './../../navcontent/navcontent.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileRoutingModule } from './profile-routing.module';
import { ProfilepageComponent } from './profilepage/profilepage.component';


@NgModule({
  declarations: [
    ProfilepageComponent
  ],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    NavcontentModule
  ]
})
export class ProfileModule { }
