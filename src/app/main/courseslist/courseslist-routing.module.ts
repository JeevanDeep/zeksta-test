import { AddCourseslistComponent } from './component/add-courseslist.component';
import { EditCourseslistComponent } from './component/edit-courseslist.component';
import { ListCourseslistComponent } from './component/list-courseslist.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path:'',component:ListCourseslistComponent},
  {path:'editCourseslist',component:EditCourseslistComponent},
  {path:'addCourseslist',component:AddCourseslistComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoursesListRoutingModule { }
