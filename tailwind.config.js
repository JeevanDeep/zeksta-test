/** @type {import('tailwindcss').Config} */
module.exports = {
 content: ['./src/**/*.{html,ts}', './projects/**/*.{html,ts}'],
 theme: {
  extend: {
    fontFamily: {
      Cinzel: ['"Cinzel Decorative"']
    }
  }
 },
 plugins: [require('@tailwindcss/forms'),require('@tailwindcss/typography')],
};
