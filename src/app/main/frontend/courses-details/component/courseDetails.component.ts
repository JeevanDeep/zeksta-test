import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-courseDetails',
  templateUrl: '../template/courseDetails.component.html',
  styleUrls: ['../css/courseDetails.component.css']
})
export class CourseDetailsComponent implements OnInit {
  public tutorialId!: string;

  topic = [
    {'id':1, 'slug': 'introduction', 'name': 'Introduction'},
    {'id':2, 'slug': 'chapter-1', 'name': 'Chapter 1'},
    {'id':3, 'slug': 'chapter-2', 'name': 'Chapter 2'},
    {'id':4, 'slug': 'chapter-3', 'name': 'Chapter 3'},
    {'id':5, 'slug': 'chapter-4', 'name': 'Chapter 4'},
    {'id':6, 'slug': 'chapter-5', 'name': 'Chapter 5'},
    {'id':7, 'slug': 'chapter-6', 'name': 'Chapter 6'},
    {'id':8, 'slug': 'chapter-7', 'name': 'Chapter 7'},
    {'id':9, 'slug': 'chapter-8', 'name': 'Chapter 8'},
    {'id':10, 'slug': 'chapter-9', 'name': 'Chapter 9'},
    {'id':11, 'slug': 'chapter-10', 'name': 'Chapter 10'},
    {'id':12, 'slug': 'chapter-11', 'name': 'Chapter 11'},
  ]

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.tutorialId = params['coursedetails'];
    });
  }

}
