import { MatIconModule } from '@angular/material/icon';
import { DeleteCourseCategoriesComponent } from './component/delete-coursecategories.component';
import { AddCourseCategoriesComponent } from './component/add-coursecategories.component';
import { EditCourseCategoriesComponent } from './component/edit-coursecategories.component';
import { NavcontentModule } from './../../navcontent/navcontent.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListCourseCategoriesComponent } from './component/list-coursecategories.component';
import { CoursecategoriesRoutingModule } from './coursecategories-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule} from '@angular/material/form-field';


@NgModule({
  declarations: [
    ListCourseCategoriesComponent,
    EditCourseCategoriesComponent,
    AddCourseCategoriesComponent,
    DeleteCourseCategoriesComponent

  ],
  imports: [
    CommonModule,
    CoursecategoriesRoutingModule,
    NavcontentModule,
    MatIconModule,
    ReactiveFormsModule,
    FormsModule,
    MatFormFieldModule

  ],
  exports:[
    ListCourseCategoriesComponent,
    EditCourseCategoriesComponent,
    AddCourseCategoriesComponent,
    DeleteCourseCategoriesComponent
  ]
})
export class CoursecategoriesModule { }
