import { EditCourseCategoriesComponent } from './component/edit-coursecategories.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListCourseCategoriesComponent } from './component/list-coursecategories.component';
import { AddCourseCategoriesComponent } from './component/add-coursecategories.component';

const routes: Routes = [
  {path:'',component:ListCourseCategoriesComponent},
  {path:'editCoursecategories',component:EditCourseCategoriesComponent},
  {path:'addCoursecategories',component:AddCourseCategoriesComponent},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoursecategoriesRoutingModule { }
